export default {
    HomeAPI : {
        url : 'api/v1/home?',
        options: {}
    },
    ProjectListing : {
        url : 'api/v1/project/list?',
        options: {}    
    },
    BlogListing : {
        url : 'api/v1/blog/list?',
        options: {}    
    },
    BlogDetail : {
        url : 'api/v1/blog/detail?',
        options: {}    
    },
    ProjectDetail : {
        url : 'api/v1/project/detail?',
        options: {}    
    },
    LeadPostAPI : {
        url : 'api/v1/leads?',
        options: {}    
    }

}